#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static inline block_capacity block_capacity_of(size_t bytes) {
  return (block_capacity) { bytes };
}

static inline block_size block_size_of(size_t bytes) {
  return (block_size) { bytes };
}

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}
static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

static void block_init(
  void *restrict addr, 
  block_size block_size,
  void *restrict next
) {
  *((struct block_header *) addr) = (struct block_header) {
    .next = next, 
    .capacity = capacity_from_size(block_size), 
    .is_free = true
  };
}

static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

void *map_pages(
  void const *addr, 
  size_t length, 
  int additional_flags
) {
  return mmap(
    (void *)addr, 
    length, 
    PROT_READ | PROT_WRITE,
    MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 
    -1, 
    0
  );
}

void * mmap_continuing_region_if_possible(void const *addr, size_t size) {
  void *pages = map_pages(addr, size, MAP_FIXED_NOREPLACE);
  if (pages != MAP_FAILED) {
    return pages;
  }
  return map_pages(addr, size, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *preferred_addr, size_t query) {
  size_t region_size = region_actual_size(
    size_from_capacity(block_capacity_of(query)).bytes
  );
  void *allocated_region =
      mmap_continuing_region_if_possible(preferred_addr, region_size);
  if (allocated_region == MAP_FAILED) {
    return REGION_INVALID;
  }
  struct region region = (struct region) {
    .addr = allocated_region,
    .size = region_size,
    .extends = preferred_addr == allocated_region
  };
  block_init(region.addr, block_size_of(region.size), NULL);
  return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
  const struct region region = 
    alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_is_splittable(
  struct block_header *restrict block,
  size_t query
) {
  return block->is_free 
    && query 
    + offsetof(struct block_header, contents) 
    + BLOCK_MIN_CAPACITY 
    <= block->capacity.bytes;
}

static void split_block(struct block_header *block, size_t query) {
  struct block_header *new_next_block_address = 
    (struct block_header *) block->contents + query;
  block_init(
    new_next_block_address,
    block_size_of(block->capacity.bytes - query), 
    block->next
  );
  block->capacity = block_capacity_of(query);
  block->next = new_next_block_address;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  bool block_is_too_big = block_is_splittable(block, query);
  if (block_is_too_big) {
    split_block(block, query);
  }
  return block_is_too_big;
}

/*  --- Слияние соседних свободных блоков --- */
static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(
  struct block_header const *first,
  struct block_header const *second
) {
  return (void *)second == block_after(first);
}

static bool mergeable(
  struct block_header const *restrict first,
  struct block_header const *restrict second
) {
  return first != NULL 
    && second != NULL 
    && first->is_free 
    && second->is_free 
    && blocks_continuous(first, second);
}
static bool try_merge_with_next(struct block_header *block) {
  bool can_be_merged = mergeable(block, block->next);
  if (can_be_merged) {
    block->capacity.bytes += 
      size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
  }
  return can_be_merged;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { 
    BSR_FOUND_GOOD_BLOCK, 
    BSR_REACHED_END_NOT_FOUND, 
    BSR_CORRUPTED 
  } type;
  struct block_header *block;
};

static inline bool is_good_block(
  struct block_header *restrict block,
  size_t size
) {
  return block->is_free 
    && block_is_big_enough(size, block);
}

static inline bool is_corrupted_block(
  struct block_header *restrict block
) {
  return block->next == block;
}

static struct block_search_result
find_good_or_last(struct block_header *restrict block, size_t sz) {
  while (true) {
    while (try_merge_with_next(block)) {
      // Merging until we can't
    }
    if (is_good_block(block, sz)) {
      return (struct block_search_result){
        .type = BSR_FOUND_GOOD_BLOCK,
        .block = block
      };
    }
    if (is_corrupted_block(block)) {
      return (struct block_search_result){
        .type = BSR_CORRUPTED,
        .block = block
      };
    }
    if (!block->next) {
      return (struct block_search_result){
        .type = BSR_REACHED_END_NOT_FOUND,
        .block = block
      };
    }
    block = block->next;
  }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  struct block_search_result result = 
    find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;
}

static struct block_header *grow_heap(
  struct block_header *restrict last,
  size_t query
) {
  uint8_t* preffered_address = last->contents + last->capacity.bytes;
  struct region region = alloc_region(preffered_address, query);
  if (!region_is_invalid(&region) && region.extends && last->is_free) {
    last->capacity.bytes += region.size;
    return last;
  }
  last->next = region.addr;
  return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result;
  while (
    (result = try_memalloc_existing(query, heap_start))
      .type == BSR_REACHED_END_NOT_FOUND
  ) {
    if (grow_heap(result.block, query) == NULL) {
      return NULL;
    }
  }
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    return result.block;
  }
  return NULL;
}

void *_malloc(size_t query) {
  struct block_header *const addr =
      memalloc(query, (struct block_header *) HEAP_START);
  if (addr)
    return addr->contents;
  return NULL;
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(
    ((uint8_t *)contents) 
     - offsetof(struct block_header, contents)
  );
}

void _free(void *mem) {
  if (!mem) return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header)) {
    // Merging until we can't
  }
}
